import aoc.puzzles.puzzle_01


def test_01() -> None:
    assert aoc.puzzles.puzzle_01.main(2) == 55776


def test_02() -> None:
    assert aoc.puzzles.puzzle_01.main(3) == 223162626
