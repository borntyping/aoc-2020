import aoc.input


def one(right: int = 3, down: int = 1) -> int:
    lines = aoc.input.lines("03.txt")
    width = len(lines[0])

    x, y = 0, 0
    trees = 0

    while True:
        x += right
        y += down

        if y >= len(lines):
            break

        if lines[y][x % width] == "#":
            trees += 1

    return trees


def two() -> int:
    return one(1, 1) * one(3, 1) * one(5, 1) * one(7, 1) * one(1, 2)


if __name__ == "__main__":
    print(one())
    print(two())
