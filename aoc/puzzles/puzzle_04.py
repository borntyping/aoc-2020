"""
Passport data is validated in batch files (your puzzle input). Each passport is
represented as a sequence of key:value pairs separated by spaces or newlines. Passports
are separated by blank lines.

Here is an example batch file containing four passports:

ecl:gry pid:860033327 eyr:2020 hcl:#fffffd byr:1937 iyr:2017 cid:147 hgt:183cm

iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884 hcl:#cfa07d byr:1929

hcl:#ae17e1 iyr:2013 eyr:2024 ecl:brn pid:760753108 byr:1931 hgt:179cm

hcl:#cfa07d eyr:2025 pid:166559648 iyr:2011 ecl:brn hgt:59in

The first passport is valid - all eight fields are present. The second passport is
invalid - it is missing hgt (the Height field).

The third passport is interesting; the only missing field is cid, so it looks like data
from North Pole Credentials, not a passport at all! Surely, nobody would mind if you
made the system temporarily ignore missing cid fields. Treat this "passport" as valid.

The fourth passport is missing two fields, cid and byr. Missing cid is fine, but missing
any other field is not, so this passport is invalid.

According to the above rules, your improved system would report 2 valid passports.

Count the number of valid passports - those that have all required fields. Treat cid as
optional. In your batch file, how many passports are valid?
"""
import re

import aoc.input

required = {"byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid", "cid"}
modified = {"byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"}


def one():
    return len(
        [
            passport
            for passport in [
                dict(pair.split(":", maxsplit=1) for pair in entry.split())
                for entry in aoc.input.path("04.txt").read_text().split("\n\n")
            ]
            if set(passport.keys()) == required or set(passport.keys()) == modified
        ]
    )


def two():
    """
    The line is moving more quickly now, but you overhear airport security talking about
    how passports with invalid data are getting through. Better add some data
    validation, quick!

    You can continue to ignore the cid field, but each other field has strict rules
    about what values are valid for automatic validation:

    byr (Birth Year) - four digits; at least 1920 and at most 2002.
    iyr (Issue Year) - four digits; at least 2010 and at most 2020.
    eyr (Expiration Year) - four digits; at least 2020 and at most 2030.
    hgt (Height) - a number followed by either cm or in:
        If cm, the number must be at least 150 and at most 193.
        If in, the number must be at least 59 and at most 76.
    hcl (Hair Color) - a # followed by exactly six characters 0-9 or a-f.
    ecl (Eye Color) - exactly one of: amb blu brn gry grn hzl oth.
    pid (Passport ID) - a nine-digit number, including leading zeroes.
    cid (Country ID) - ignored, missing or not.

    Your job is to count the passports where all required fields are both present and
    valid according to the above rules. Here are some example values:
    """
    return len(
        [
            passport
            for passport in [
                dict(pair.split(":", maxsplit=1) for pair in entry.split())
                for entry in aoc.input.path("04.txt").read_text().split("\n\n")
            ]
            if (set(passport.keys()) == required or set(passport.keys()) == modified)
            and (1920 <= int(passport["byr"]) <= 2002)
            and (2010 <= int(passport["iyr"]) <= 2020)
            and (2020 <= int(passport["eyr"]) <= 2030)
            and (
                (150 <= int(passport["hgt"][:-2]) <= 193)
                if passport["hgt"][-2:] == "cm"
                else (
                    59 <= int(passport["hgt"][:-2]) <= 76
                    if passport["hgt"][-2:] == "in"
                    else False
                )
            )
            and (re.match(r"^#[0-9a-f]{6}$", passport["hcl"]))
            and (passport["ecl"] in {"amb", "blu", "brn", "gry", "grn", "hzl", "oth"})
            and (re.match(r"^\d{9}$", passport["pid"]))
        ]
    )


if __name__ == "__main__":
    print(one())
    print(two())
