import re

import typing

import aoc.input


def one() -> typing.Iterable[str]:
    lines = aoc.input.lines("02.txt")
    regex = re.compile("^(\d+)-(\d+) ([a-z]+): (\w+)$")

    for line in lines:
        match = regex.search(line)
        min_count_s, max_count_s, letter, password = match.groups()
        min_count, max_count = int(min_count_s), int(max_count_s)
        count = len([c for c in password if c == letter])
        if min_count <= count <= max_count:
            yield password


def two() -> typing.Iterable[str]:
    lines = aoc.input.lines("02.txt")
    regex = re.compile("^(\d+)-(\d+) ([a-z]+): (\w+)$")

    for line in lines:
        match = regex.search(line)
        first_s, second_s, letter, password = match.groups()
        first, second = int(first_s), int(second_s)

        f = password[first - 1] == letter
        s = password[second - 1] == letter

        if f and s:
            pass
        elif f or s:
            yield password


if __name__ == "__main__":
    print(len(list(one())))
    print(len(list(two())))
