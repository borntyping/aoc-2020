import functools
import operator

import aoc.input
import itertools


def main(r: int) -> int:
    numbers = aoc.input.numbers("01.txt")
    for entries in itertools.permutations(numbers, r):
        if sum(entries) == 2020:
            return functools.reduce(operator.mul, entries)


if __name__ == "__main__":
    print(main(2))
    print(main(3))
