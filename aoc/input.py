import typing
import pathlib


def path(name: str) -> pathlib.Path:
    return pathlib.Path(__file__).with_name("fixtures").joinpath(name)


def lines(name: str) -> typing.Sequence[str]:
    return path(name).read_text().splitlines()


def numbers(name: str) -> typing.Sequence[int]:
    return [int(line) for line in lines(name)]
